'use strict';

var fs = require("fs"),
  extend = require("extend"),
  dateFormat = require('dateformat'),
  async = require('async'),
  spawn = require("child_process").spawn,
  jsm = require("javascript-state-machine");

function cascadecollage (opts) {

  var that = {};

  var default_opts =   {
      archive: "archive.json",
      record_frames: 20, /* number of frames to record from the camera to make loops */
      min_faces: 5, /* minimum number of detected faces to accept a recording */
      camera: 0,
      shuffle_archive: true,
      collage_match_time: 2000, /* time between feature selection from archive */
      camera_webm_framerate: 12,
      archive_webm_framerate: 1,
      collage_webm_framerate: 12,
      record_countdown_time: 3000,
      loop_timeout: 30*1000 /* delay before looping to start watching again */
    };

  opts = extend ({}, default_opts, opts);


  // some helper functions....

  function shuffle(o) {
      for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
      return o;
  }

  function loopiter (items) {
    // an infinite looping iterator for a list
    var index = 0,
      that = {};
      that.next = function () {
        var ret = items[index];
        index += 1;
        if (index == items.length) {
          index = 0;
        }
        return ret;
      }
      return that;
  }

  function rwalk (node, callback) {
    if (Array.isArray(node)) {
      for (var i=0, l=node.length; i<l; i++) {
        rwalk(node[i], callback);
      }
    } else {
      callback.call(node, node);
    }
  }

  function rget (node, feature_name, key) {
    if (Array.isArray(node)) {
      for (var i=0, l = node.length; i<l; i++) {
        var ret = rget(node[i], feature_name, key);
        if (ret !== undefined) { return ret }
      }
    } else if (node.name == feature_name) {
      return node[key];
    }
  }

  function get_feature_color (feature_name) {
    return rget(opts.features, feature_name, "color") || "255,255,255";
  }

  function get_feature_width (feature_name) {
    return rget(opts.features, feature_name, "width") || "2";
  }

  // console.log("get_feature_color.faces", get_feature_color("faces"))
  // console.log("get_feature_color.eyes", get_feature_color("eyes"))
  // console.log("get_feature_color.bodies", get_feature_color("bodies"))


  function _archive (data) {
    // wrapper for the archive data, provides next_face & next_eye methods
    // console.log("archive data", data, data.length);
    // wait for start signal...
    var images = data;

    if (opts.shuffle_archive) {
      shuffle(data);
    }

    var numimages = data.length,
      features = {};

    for (var i=0, l=images.length; i<l; i++) {
      for (var key in images[i].features) {
        if (features[key] === undefined) {
          features[key] = [];
        }
        images[i].features[key].forEach(function (rect) {
          features[key].push({
            feature: key,
            path: images[i].path,
            rect: rect
          })
        })
      }
    }

    if (opts.shuffle_archive) {
      for (var key in features) {
        shuffle(features[key]);
      }
    }

    var that = {},
      iterators = {};

    for (var key in features) {
      iterators[key] = loopiter(features[key]);
    }

    that.next_feature = function (key) {
      return iterators[key].next();
    }
    that.unparse = function () {
      var msg = "";
      for (var key in features) {
        msg += msg ? ", " : "";
        msg += key + " " + features[key].length;
      }
      return msg;
    }

    return that;
  }

  /* load the archive */
  var archive = _archive(JSON.parse(fs.readFileSync(opts.archive))),
    cameraimages = [],
    imagedata = [],

    // facecount
    featurecount = 0,

    annotated_camera_frames = [],

    // collage_faces = [],
    // collage_eyes = [],
    collage_features = {},

    archive_frames = [],
    collage_base,
    cameragif,
    archivegif,
    collagegif;

  var ordered_features = [];
  rwalk(opts.features, function (n) {
    ordered_features.push(n.name);
  })
  console.log("Feature order:", ordered_features);
  console.log("Using "+ opts.archive);
  console.log("Archive has", archive.unparse());

  var fsm = jsm.create({
      initial: 'watch',
      events: [
        { name: 'start_recording', from: 'watch', to: 'record' },
        { name: 'start_analyzing', from: 'record', to: 'analyze' },
        { name: 'accept_analysis', from: 'analyze', to: 'collage'},
        // { name: 'build_collage', from: 'match', to: 'collage'},
        { name: 'reject_analysis', from: 'analyze', to: 'watch'},
        { name: 'start_cleanup', from: 'collage', to: 'cleanup' },
        { name: 'start_watching', from: 'cleanup', to: 'watch' }
      ],
      callbacks: {

        //                _       _     
        // __      ____ _| |_ ___| |__  
        // \ \ /\ / / _` | __/ __| '_ \ 
        //  \ V  V / (_| | || (__| | | |
        //   \_/\_/ \__,_|\__\___|_| |_|

        onwatch: function (event, from, to) {
          if (opts.watch) {
            opts.watch.call(that);
          }
          console.log("WATCHING");
          console.log("scripts/waitforfeature");
          var p = spawn("scripts/waitforfeature", ["--camera", opts.camera] );
          p.stdout.setEncoding('utf8');
          p.stderr.setEncoding('utf8');
          p.stderr.on("data", function (data) {
            console.log("waitforfeature, stderr:", data);
          })
          // scan.stdout.on("data", function (data) {
          // });
          p.on("close", function (code) {
            if (code !== 0) { console.log("waitforfeature returned", code); }
            // trigger countdown event...
            if (opts.countdown) {
              opts.countdown.call(that);
            }
            setTimeout(function () {
              fsm.start_recording();
            }, opts.record_countdown_time)
          })
        },

        //                             _ 
        //  _ __ ___  ___ ___  _ __ __| |
        // | '__/ _ \/ __/ _ \| '__/ _` |
        // | | |  __/ (_| (_) | | | (_| |
        // |_|  \___|\___\___/|_|  \__,_|

        onrecord: function (event, from, to) {
          if (opts.record) {
            opts.record.call(that);
          }
          console.log("RECORDING");
          console.log("scripts/record");
          var p = spawn("scripts/record", ["--frames", opts.record_frames, "--camera", opts.camera] );
          cameraimages = [];
          p.stderr.setEncoding('utf8');
          p.stderr.on("data", function (data) {
            console.log("record, stderr:", data);
          })
          p.stdout.setEncoding('utf8');
          p.stdout.on("data", function (data) {
            var images = data.trim().split(/\n/);
            // console.log("record, data", images);
            images.forEach(function (x) {
              // socket.emit("cameraimage", {image: x});
              // console.log("cameraimage", x);
              if (opts.cameraimage) {
                opts.cameraimage.call(that, x);
              }
              cameraimages.push(x);
            })
          });
          p.on("close", function (code) {
            if (code !== 0) { console.log("record returned", code); }
            fsm.start_analyzing();
          })
        },

        //                    _               
        //   __ _ _ __   __ _| |_   _ _______ 
        //  / _` | '_ \ / _` | | | | |_  / _ \
        // | (_| | | | | (_| | | |_| |/ /  __/
        //  \__,_|_| |_|\__,_|_|\__, /___\___|
        //                      |___/         
        // & match
        //
        // calls scripts/getfaces for each camera frame
        // resets the collection of faces & eyes
        // MATCHES camera features with those in the archive
        // nb: each frame is considered separately
        // and only enough faces & eyes are collected
        // to match the MAXIMUM number of faces or eyes across all frames
        // ( ie collage_faces.length == max(frame.faces.length) for all frames )

        onanalyze: function (event, from, to) {
          console.log("ANALYZING");
          if (opts.analyze) {
            opts.analyze.call(that);
          }
          imagedata = [];
          featurecount = 0;

          annotated_camera_frames = [];
          collage_features = {};
          // collage_faces = [];
          // collage_eyes = [];

          archive_frames = [];

          async.eachSeries(cameraimages, function (path, callback) {
              var output = '';
              console.log("scripts/getfeatures", path);
              var faces = spawn("scripts/getfeatures", [path]);
              faces.stdout.setEncoding("utf8");
              faces.stdout.on("data", function (text) {
                output += text;
              });
              faces.on("close", function (code) {
                if (code !== 0) { console.log("getfeatures returned", code); }
                var data = JSON.parse(output)[0];
                console.log("data", data);
                imagedata.push(data);
                if (opts.imagedata) {
                  opts.imagedata.call(that, data);
                }
                var frame_feature_count = 0;
                for (var key in data.features) {
                  featurecount += data.features[key].length;
                  frame_feature_count += data.features[key].length;
                }

                if (frame_feature_count) {
                  var outpath = path.replace(/^frames\/frame/, "frames/camera");
                  annotated_camera_frames.push(outpath);
                  draw_features(path, data, outpath, function () {
                    // EVENT: annotatedcameraframe
                    if (opts.annotatedcameraframe) {
                      opts.annotatedcameraframe.call(that, outpath);
                    }
                    process_frame(path, data, callback);
                  })
                } else {
                  setTimeout(callback, 0);
                }
              });
          }, function (err) {
              console.log("end of face detection loop, found "+featurecount+" features");
              if (featurecount > opts.min_faces) {
                fsm.accept_analysis();
              } else {
                // remove the camera frames... actually should cleanup all files (!?)
                async.each(cameraimages, fs.unlink);
                fsm.reject_analysis();
              }
          });

          function draw_features(img, data, outpath, onend) {
            var p, args = [img, outpath];
            for (var key in data.features) {
              data.features[key].forEach(function (x) {
                args.push("--rect");
                args.push(""+x[0]+","+x[1]+","+x[2]+","+x[3]);
                args.push(get_feature_color(key))
                args.push(get_feature_width(key));                
              })
            }
            // console.log("annotateimage", args);
            p = spawn("scripts/annotateimage", args);
            p.on("close", function (code) {
              if (code !== 0) { console.log("annotateimage returned", code); }
              onend.call(this);
            });
          }

          function process_frame(img, data, onend) {

            // double duty: calculates data.features_flat FOR ALL features
            // NB order of features_flat is ALSO the draw order...
            // should keep the feature order from settings (bodies, faces, eyes)
            // thus uses ordered_features
            // and calculates an "order" based on the current (collage_)faces & eyes
            // calculate the "order" of faces + eyes to retrieve
            // then process them one by one in turn...

            var order = [], match_count = false;
            data.features_flat = [];
            ordered_features.forEach(function (key) {
              if (data.features[key]) {
                data.features[key].forEach(function (rect, i) {
                  var f = {
                    feature: key,
                    path: data.path,
                    rect: rect
                  };
                  if (collage_features[key] === undefined) { collage_features[key] = [] }
                  if (i>=collage_features[key].length) { order.push(f); }
                  data.features_flat.push(f);
                });                
              }
            });
            // if (data.faces && data.faces.length) {
            //   data.faces.forEach(function (rect, i) {
            //     var f = {
            //       feature: "face",
            //       path: data.path,
            //       rect: rect
            //     };
            //     if (i>=collage_faces.length) { order.push(f); }
            //     data.features.push(f);
            //   })
            // }
            // if (data.eyes && data.eyes.length) {
            //   data.eyes.forEach(function (rect, i) {
            //     var f = {
            //       feature: "eye",
            //       path: data.path,
            //       rect: rect
            //     };
            //     if (i>=collage_eyes.length) { order.push(f) }
            //     data.features.push(f);
            //   });
            // }
            match_count = order.length;
            // console.log("collage: order length", order.length);

            async.eachSeries(order, function (o, callback) {
              var f = archive.next_feature(o.feature);
              collage_features[o.feature].push(f);
              // if (o.feature == "face") {
              //   f = archive.next_face();
              //   collage_faces.push(f);
              // } else {
              //   f = archive.next_eye();
              //   collage_eyes.push(f);
              // }
              console.log("MATCH", o, "to", f);
              // record / draw archive frame with feature box
              var output = "frames/archive"+zeropad(archive_frames.length, 4)+".jpg";
              // mkarchiveframe(f.path, f.rect, f.feature == "face" ? "blue" : "green", output, function () {
              mkarchiveframe(f.path, f.rect, get_feature_color(o.feature), output, function () {
                archive_frames.push(output);
                if (opts.archiveframe) {
                  opts.archiveframe.call(that, output, o, f);
                }
                setTimeout(callback, opts.collage_match_time);
              });

            }, onend);

          }

        },

        //            _ _                  
        //   ___ ___ | | | __ _  __ _  ___ 
        //  / __/ _ \| | |/ _` |/ _` |/ _ \
        // | (_| (_) | | | (_| | (_| |  __/
        //  \___\___/|_|_|\__,_|\__, |\___|
        //                      |___/      

        oncollage: function (event, from, to) {
          if (opts.collage) {
            opts.collage.call(that);
          }
          console.log("collage");
          var now = new Date();
          collage_base = dateFormat(now, "yyyymmdd_HHMMss");
          cameragif = "output/camera"+collage_base+".webm";
          //mkwebm(annotated_camera_frames, cameragif, opts.camera_webm_framerate, true, function () {
          //  if (opts.cameragif) {
          //    opts.cameragif.call(that, cameragif);
          //  }
            archivegif = "output/archive"+collage_base+".webm";
            mkwebm(archive_frames, archivegif, opts.archive_webm_framerate, false, function () {
              if (opts.archivegif) {
                opts.archivegif.call(that, archivegif);
              }

              // BUILD FRAMES
              console.log("COLLAGE, building frames");
              var collage_frames = [];
              // create the actual collage frames!!
              // collect all the source faces & eyes (per frame)
              // console.log("collage features:", featurecount);
              var frames = imagedata.filter(function (x) { return x.features_flat && x.features_flat.length > 0 });
              function next_frame () {
                var feature_index = {};
                // var face_index = 0, eye_index = 0;
                if (frames.length == 0) {
                  // END...
                  // build GIFs
                  collagegif = "output/collage"+collage_base+".webm";
                  mkwebm(collage_frames, collagegif, opts.collage_webm_framerate, true, function () {
                    if (opts.collagegif) {
                      opts.collagegif.call(that, collagegif);
                    }
                    fsm.start_cleanup();
                  });
                  return;
                }
                var frame = frames.shift(),
                  features = frame.features_flat.slice();

                // output frame has a name
                var outputframe = frame.path.replace(/frames\/frame/, 'frames/collage'),
                  first_output = true;

                collage_frames.push(outputframe);
                
                function next_feature () {
                  if (features.length == 0) {
                    next_frame();
                    return;
                  }
                  // PASTE FEATURE
                  var f = features.shift(),
                    archive_f;
                  if (feature_index[f.feature] === undefined) {
                    feature_index[f.feature] = 0;
                  }
                  archive_f = collage_features[f.feature][feature_index[f.feature]++];
                  collage(archive_f.path, archive_f.rect, first_output ? frame.path : outputframe, f.rect, outputframe, function () {
                    next_feature();
                  });

                  // if (f.feature == "face") {
                  //   // console.log("face:", face_index);
                  //   archive_f = collage_faces[face_index++];
                  //   collage(archive_f.path, archive_f.rect, first_output ? frame.path : outputframe, f.rect, outputframe, function () {
                  //     next_feature();
                  //   });
                  // } else {
                  //   archive_f = collage_eyes[eye_index++];
                  //   collage(archive_f.path, archive_f.rect, first_output ? frame.path : outputframe, f.rect, outputframe, function () {
                  //     next_feature();
                  //   });
                  // }
                  first_output = false;
                }
                next_feature();
              }
              next_frame();
            });
          // })
        },

        //       _                              
        //   ___| | ___  __ _ _ __  _   _ _ __  
        //  / __| |/ _ \/ _` | '_ \| | | | '_ \ 
        // | (__| |  __/ (_| | | | | |_| | |_) |
        //  \___|_|\___|\__,_|_| |_|\__,_| .__/ 
        //                               |_|    
        // run cleanup.sh
        // ( which empties the frames path
        //   and copies output gifs to the server ) 
        // after opts.loop_timeout, return to WATCH

        oncleanup: function (event, from, to) {
          if (opts.cleanup) {
            opts.cleanup.call(that);
          }
          console.log("CLEANUP");
          console.log("scripts/cleanup", cameragif, archivegif, collagegif);
          var cmd = spawn("scripts/cleanup", [cameragif, archivegif, collagegif]);
          cmd.on("close", function (code) {
            if (code) { console.log("cleanup returned", code); }
            setTimeout(function () {
              fsm.start_watching();
            }, opts.loop_timeout);
          })
        }

      }
    });

  function im_geo(rect) {
    // imagemagick's idiosyncratic geometry format
    var x= rect[0],
      y = rect[1],
      w = rect[2],
      h = rect[3];
    return ""+w+"x"+h+"+"+x+"+"+y;
  }

  var collage_count = 0;

  function zeropad (n, places) {
    n = ""+n;
    while (n.length<places) { n = "0"+n; }
    return n;
  }

  function mkwebm (paths, output, framerate, reverse, onend) {
    // convert -loop 0 -delay 500 output/archive* archive.gif

    var args = ["--output", "output.avi", "--framerate", framerate],
      cmd;

    if (reverse) {
      args.push("--reverseloop")
    } else {
      args.push("--repeatlast");
    }
    paths.forEach(function (x) { args.push(x) });
    cmd = spawn("scripts/mkavi", args);
    cmd.on("close", function (code) {
      if (code != 0) { console.log("./mkavi.sh returned", code); }
      var p = spawn("scripts/avi2webm", ["output.avi", output]);
      p.on("close", function (code) {
        if (code != 0) { console.log("./avi2webm.sh (convert) returned", code); }
        onend.call(this);
      })
    })
  }

  function mkarchiveframe (srcpath, srcrect, color, output, onend) {
    if (!isNaN(parseInt(color))) {
      color = "rgb("+color+")"
    }
    console.log("scripts/archiveframe", srcpath, srcrect, color, output);
     // convert output.png -fill none -stroke green -strokewidth 3 -draw "rectangle 60,10 200,20" output.png
    color = color || "blue";
    // var cmd = spawn("convert", [srcpath, "-fill", "none", "-stroke", color, "-strokewidth", "2", "-draw", "rectangle "+srcrect[0]+","+srcrect[1]+" "+(srcrect[0]+srcrect[2])+","+(srcrect[1]+srcrect[3]), output]);
    var shape = "rectangle "+srcrect[0]+","+srcrect[1]+" "+(srcrect[0]+srcrect[2])+","+(srcrect[1]+srcrect[3]),
      cmd = spawn("scripts/archiveframe", [srcpath, shape, color, output]);
    cmd.stderr.setEncoding("utf8");
    cmd.on("close", function (code) {
      if (code !== 0) { console.log("archiveframe returned", code); }
      // if (code == 0) 
      onend.call(this);
    });
    cmd.stderr.on("data", function (data) {
      console.log("stderr", data);
    });
  }

  function collage (srcpath, srcrect, destpath, destrect, output, onend) {
    // map same inputs to same outputs... and collect all the (unique) output paths
    console.log("collage", srcpath, srcrect, destpath, destrect, output);
    var cmd = spawn("scripts/collage", [srcpath, im_geo(srcrect), destpath, im_geo(destrect), output]);
    cmd.stdout.setEncoding("utf8");
    cmd.on("close", function (code) {
      if (code !== 0) { console.log("collage returned", code); }
      onend.call(this);
    })
  }

}

module.exports = cascadecollage;
