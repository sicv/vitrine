!function() {
  var cc = {
    version: "0.5.0"
  };
  cc.canvas = {};
  cc.canvas.image = cc_canvas_image;
  function cc_canvas_image(src, opts) {
    var _image = {}, imgelt = new Image();
    opts = opts == undefined ? {} : opts;
    _image.elt = imgelt;
    imgelt.addEventListener("load", function() {
      _image.rect = cc.rect(0, 0, imgelt.width, imgelt.height);
      if (opts.load) {
        opts.load.call(_image);
      }
    });
    function load(src2) {
      src = src2 || src;
      imgelt.src = src;
      return _image;
    }
    _image.load = load;
    function draw(ctx, src, dest, fast_p) {
      if (fast_p) {
        ctx.beginPath();
        ctx.moveTo(dest.x, dest.y);
        ctx.lineTo(dest.x + dest.width, dest.y);
        ctx.lineTo(dest.x + dest.width, dest.y + dest.height);
        ctx.lineTo(dest.x, dest.y + dest.height);
        ctx.lineTo(dest.x, dest.y);
        ctx.lineTo(dest.x + dest.width, dest.y + dest.height);
        ctx.moveTo(dest.x + dest.width, dest.y);
        ctx.lineTo(dest.x, dest.y + dest.height);
        ctx.stroke();
      } else {
        ctx.drawImage(imgelt, src.x, src.y, src.width, src.height, dest.x, dest.y, dest.width, dest.height);
      }
    }
    _image.draw = draw;
    return _image;
  }
  cc.canvas.layer = cc_canvas_layer;
  function cc_canvas_layer() {
    var _layer = {}, items = [];
    function place(item, src, dest) {
      items.push({
        item: item,
        src: src,
        dest: dest
      });
    }
    _layer.place = place;
    function intersection(src) {
      return items.filter(function(x) {
        var ret = cc.rect_intersects(src, x.dest);
        return ret;
      });
    }
    function draw(ctx, src, dest, fast_p) {
      var items = intersection(src), transform = cc.transform_from(src, dest);
      items.forEach(function(x) {
        x.item.draw(ctx, x.src, transform(x.dest), fast_p);
      });
    }
    _layer.draw = draw;
    return _layer;
  }
  cc.canvas.drawcorners = function() {
    var m = 0, a = 20;
    function arrow() {
      this.beginPath();
      this.lineWidth = 5;
      this.moveTo(m + a, m);
      this.lineTo(m, m);
      this.lineTo(m, m + a);
      this.stroke();
      this.moveTo(m, m);
      this.lineTo(a, a);
      this.stroke();
    }
    arrow.call(this);
    this.save();
    this.translate(W, 0);
    this.scale(-1, 1);
    arrow.call(this);
    this.restore();
    this.save();
    this.translate(W, H);
    this.scale(-1, -1);
    arrow.call(this);
    this.restore();
    this.save();
    this.translate(0, H);
    this.scale(1, -1);
    arrow.call(this);
    this.restore();
  };
  cc.extend = cc_extend;
  function cc_extend() {
    for (var i = 1; i < arguments.length; i++) for (var key in arguments[i]) if (arguments[i].hasOwnProperty(key)) arguments[0][key] = arguments[i][key];
    return arguments[0];
  }
  cc.window_width = cc_window_width;
  cc.window_height = cc_window_height;
  function cc_window_width() {
    var w = window, d = document, e = d.documentElement, g = d.getElementsByTagName("body")[0];
    return w.innerWidth || e.clientWidth || g.clientWidth;
  }
  function cc_window_height() {
    var w = window, d = document, e = d.documentElement, g = d.getElementsByTagName("body")[0];
    return w.innerHeight || e.clientHeight || g.clientHeight;
  }
  cc.cells = cc_cells;
  function cc_cells(opts) {
    opts = extend({}, {
      place: function(x, y, w, h) {
        this.style.left = x + "px";
        this.style.top = y + "px";
        this.style.width = w + "px";
        this.style.height = h + "px";
      }
    }, opts);
    var that = {}, occupied_cells = [], gridsize = 0, grid = [];
    function resize_grid() {
      for (var y = 0; y < gridsize; y++) {
        var row = grid[y];
        if (row === undefined) {
          row = grid[y] = [];
        }
        for (var x = 0; x < gridsize; x++) {
          var cell = row[x];
          if (cell === undefined) {
            cell = row[x] = {
              x: x,
              y: y,
              elt: null
            };
          }
        }
      }
    }
    function free_cell() {
      for (var r = 0; r < gridsize; r++) {
        var row = grid[r];
        for (var c = 0; c < gridsize; c++) {
          var cell = row[c];
          if (cell.elt === null) {
            return cell;
          }
        }
      }
      gridsize += 1;
      console.log("expanded gridsize", gridsize);
      resize_grid();
      return free_cell();
    }
    that.add = function(elt) {
      var cell = free_cell();
      cell.elt = elt;
      occupied_cells.push(cell);
      return that;
    };
    that.remove = function(elt) {
      for (var i = 0; i < occupied_cells.length; i++) {
        var cell = occupied_cells[i];
        if (cell.elt == elt) {
          cell.elt = null;
          occupied_cells.splice(i, 1);
          return that;
        }
      }
      return that;
    };
    function layout() {
      var ww = window_width(), wh = window_height(), cellwidth = ww / gridsize, cellheight = wh / gridsize;
      for (var r = 0; r < gridsize; r++) {
        var row = grid[r];
        for (var c = 0; c < gridsize; c++) {
          var cell = row[c];
          if (cell.elt !== null) {
            opts.place.call(cell.elt, cell.x * cellwidth, cell.y * cellheight, cellwidth, cellheight);
          }
        }
      }
      return that;
    }
    that.layout = layout;
    function get() {
      if (arguments.length == 0) {
        return occupied_cells.map(function(x) {
          return x.elt;
        });
      } else {
        var i = arguments[0];
        if (i >= 0) {
          return occupied_cells[i].elt;
        } else {
          return occupied_cells[occupied_cells.length + i].elt;
        }
      }
    }
    that.get = get;
    return that;
  }
  cc.ff = cc_ff;
  function cc_ff(n, maxplaces) {
    maxplaces = maxplaces == undefined ? 3 : maxplaces;
    var ret = "" + n, dp = ret.indexOf(".");
    if (dp !== -1) {
      return ret.substring(0, dp + 1 + maxplaces);
    }
    return ret;
  }
  cc.timecode = cc_timecode;
  cc.seconds_to_timecode = cc_seconds_to_timecode;
  function cc_timecode(tc) {
    var pat = /(?:(\d\d)\:)?(\d\d)\:(\d\d)(?:[,.](\d{1,3}))?/, m = pat.exec(tc);
    if (m !== null) {
      var ret = {
        hh: m[1] !== undefined ? parseInt(m[1]) : 0,
        mm: parseInt(m[2]),
        ss: parseInt(m[3]),
        ff: m[4] !== undefined ? parseFloat("." + m[4]) : 0
      };
      ret.toString = function() {
        return "[" + this.hh + ":" + this.mm + ":" + this.ss + "." + this.ff + "] " + this.value;
      };
      ret.value = ret.hh * 3600 + ret.mm * 60 + ret.ss + ret.ff;
      return ret;
    }
  }
  function cc_seconds_to_timecode(s, format) {
    var hh = Math.floor(s / 3600), mm, ss, ff, ret;
    s -= hh * 3600;
    mm = Math.floor(s / 60);
    s -= mm * 60;
    ss = Math.floor(s);
    ff = s - ss;
    ret = (hh < 10 ? "0" : "") + hh + ":";
    ret += (mm < 10 ? "0" : "") + mm + ":";
    ret += (ss < 10 ? "0" : "") + ss;
    if (ff > 0) {
      ret += "." + ff.toString().substring(2, 5);
    }
    return ret;
  }
  cc.href = cc_href;
  var cc_t_pat = /^t=((?:\d\d\:)?\d\d\:\d\d(?:[,.]\d{1,3})?)(?:,((?:\d\d\:)?\d\d\:\d\d(?:[,.]\d{1,3})?))?$/, cc_xywh_pat = /^xywh=((?:pixel\:)|(?:percent\:))?(\d+(?:\.\d+)?),(\d+(?:\.\d+)?),(\d+(?:\.\d+)?),(\d+(?:\.\d+)?)$/;
  function cc_parse_fragment_component(c) {
    var m = cc_t_pat.exec(c);
    if (m !== null) {
      return {
        start: cc_timecode(m[1]),
        end: m[2] !== undefined ? cc_timecode(m[2]) : undefined
      };
    }
    m = cc_xywh_pat.exec(c);
    if (m !== null) {
      return {
        x: parseFloat(m[2]),
        y: parseFloat(m[3]),
        width: parseFloat(m[4]),
        height: parseFloat(m[5])
      };
    }
  }
  function cc_href(h) {
    var that = {}, fpos = h.indexOf("#");
    that.base = h;
    that.frag;
    if (fpos !== -1) {
      that.base = h.substring(0, fpos);
      that.frag = h.substring(fpos + 1);
      that.frag.split("&").forEach(function(nvp) {
        var pc = cc_parse_fragment_component(nvp);
        if (pc !== undefined) {
          for (var key in pc) {
            that[key] = pc[key];
          }
        }
      });
    }
    that.duration = function() {
      if (that.start && that.end) {
        return that.end.value - that.start.value;
      }
    };
    return that;
  }
  cc.rect = cc_rect;
  function cc_rect(x, y, width, height) {
    return {
      x: x,
      y: y,
      width: width,
      height: height
    };
  }
  cc.overlaps_p = cc_overlaps_p;
  function cc_overlaps_p(start, end, s, e) {
    return s >= start && s < end || e >= start && e < end || s < start && e >= end;
  }
  cc.rect_intersects = cc_rect_intersects;
  function cc_rect_intersects(a, b) {
    return cc_overlaps_p(a.x, a.x + a.width, b.x, b.x + b.width) && cc_overlaps_p(a.y, a.y + a.height, b.y, b.y + b.height);
  }
  cc.transform = cc_transform;
  function cc_transform(a, b, c, d, e, f) {
    var ret = function(x) {
      if (x.matrix) {
        return cc_transform(a * x.a + c * x.b, b * x.a + d * x.b, a * x.c + c * x.d, b * x.c + d * x.d, a * x.e + c * x.f + e, b * x.e + d * x.f + f);
      } else {
        var ret = {
          x: a * x.x + c * x.y + e,
          y: b * x.x + d * x.y + f
        };
        if (x.width !== undefined) {
          ret.width = x.width * a;
          ret.height = x.height * d;
        }
        return ret;
      }
    };
    ret.matrix = true;
    ret.a = a;
    ret.b = b;
    ret.c = c;
    ret.d = d;
    ret.e = e;
    ret.f = f;
    return ret;
  }
  cc.translate = cc_translate;
  function cc_translate(tx, ty) {
    return cc_transform(1, 0, 0, 1, tx, ty);
  }
  cc.scale = cc_scale;
  function cc_scale(sx, sy) {
    return cc_transform(sx, 0, 0, sy, 0, 0);
  }
  cc.transform_from = cc_transform_from;
  function cc_transform_from(src, dest) {
    var t1 = cc_translate(-src.x, -src.y), s1 = cc_scale(dest.width / src.width, dest.height / src.height), t2 = cc_translate(dest.x, dest.y);
    return t2(s1(t1));
  }
  cc.identity = cc_identity;
  function cc_identity() {
    return cc_transform(1, 0, 0, 1, 0, 0);
  }
  function cc_frame_rect(frame, r, center) {
    var w = frame.width, h = w * (r.height / r.width);
    if (h >= frame.height) {
      h = frame.height;
      w = h * (r.width / r.height);
    }
    var x = 0, y = 0;
    if (center) {
      x = (frame.width - w) / 2;
      y = (frame.height - h) / 2;
    }
    return cc_rect(x, y, w, h);
  }
  cc.frame_rect = cc_frame_rect;
  cc.memoize = cc_memoize;
  function cc_memoize(fn, keyfn) {
    var results = {};
    keyfn = keyfn || function(x) {
      return x.toString();
    };
    var ret = function() {
      var key = keyfn.apply(null, arguments), result = results[key];
      if (result === undefined) {
        result = fn.apply(null, arguments);
        results[key] = result;
      }
      return result;
    };
    ret.items = results;
    return ret;
  }
  cc.event_mouse_pos = cc_event_mouse_pos;
  function cc_event_mouse_pos(e, elt) {
    var posx = 0, posy = 0;
    if (!e) {
      e = window.event;
    }
    if (e.pageX || e.pageY) {
      posx = e.pageX;
      posy = e.pageY;
    } else if (e.clientX || e.clientY) {
      posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
      posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    var ret = {
      left: posx,
      top: posy
    };
    if (elt) {
      var eltpos = cc_elt_pos(elt);
      ret.left -= eltpos.left;
      ret.top -= eltpos.top;
    }
    return ret;
  }
  cc.elt_pos = cc_elt_pos;
  function cc_elt_pos(elt) {
    var r = elt.getBoundingClientRect(elt), sx = window.pageXOffset || window.scrollX, sy = window.pageYOffset || window.scrollY;
    return {
      left: r.left + sx,
      top: r.top + sy,
      right: r.right + sx,
      bottom: r.bottom + sy
    };
  }
  cc.ranger = cc_ranger;
  var cc_ranger_defaults = {
    copy: function(x) {
      if (x instanceof Date) {
        var ret = new Date();
        ret.setTime(x.getTime());
        return ret;
      } else {
        return x;
      }
    },
    test: function(start, end, s, e) {
      return s >= start && s < end || e >= start && e < end || s < start && e >= end;
    },
    debug: false
  };
  function cc_ranger(opts) {
    opts = cc_extend({}, cc_ranger_defaults, opts);
    var that = {}, min, max, _start, _end, lastStart, lastEnd, uid = 0, itemsByStart = [], itemsByEnd = [], ssi = -1, sei = -1, esi = -1, eei = -1, activeItems = {};
    that.add = function(elt, start, end, enter, exit) {
      var item = {
        id: "I" + ++uid,
        elt: elt,
        start: start,
        end: end,
        enter: enter,
        exit: exit
      };
      if (start === undefined) {
        return "no start";
      }
      if (end && end < start) {
        return "end is before start";
      }
      if (min === undefined || item.start < min) {
        min = item.start;
      }
      if (max === undefined || item.start > max) {
        max = item.start;
      }
      if (max === undefined || item.end && item.end > max) {
        max = item.end;
      }
      _insert_sorted(item, "start", itemsByStart);
      _insert_sorted(item, "end", itemsByEnd);
      if (opts.test(_start, _end, item.start, item.end)) {
        _enter(item);
        activeItems[item.id] = item;
      } else {
        _exit(item);
      }
      set(_start, _end);
    };
    function _insert_sorted(item, propname, arr) {
      var i = 0, len = arr.length;
      for (;i < len; i++) {
        if (arr[i][propname] > item[propname] || arr[i][propname] === undefined && item[propname] !== undefined) {
          arr.splice(i, 0, item);
          return;
        }
      }
      arr.push(item);
    }
    function _enter(item) {
      if (item.enter) {
        item.enter.call(item.elt);
      } else if (opts.enter) {
        opts.enter.call(item.elt);
      }
    }
    function _exit(item) {
      if (item.exit) {
        item.exit.apply(item.elt);
      } else if (opts.exit) {
        opts.exit.apply(item.elt);
      }
    }
    function nextInterestingTime() {
      var next_start = esi + 1 < itemsByStart.length ? itemsByStart[esi + 1] : undefined, next_end = eei + 1 < itemsByEnd.length ? itemsByEnd[eei + 1] : undefined, ret;
      if (next_start) {
        console.log("next_start", next_start.start, next_start.elt);
        ret = next_start.start;
      }
      if (next_end) {
        console.log("next_end", next_end.end, next_end.elt);
        ret = Math.min(ret, next_end.end);
      }
      return ret;
    }
    that.nextInterestingTime = nextInterestingTime;
    function prevInterestingTime() {
      var prev_start = ssi >= 0 && ssi < itemsByStart.length ? itemsByStart[ssi] : undefined, ret;
      if (prev_start) {
        console.log("prev_start", prev_start.start, prev_start.elt);
        ret = prev_start.start;
      }
      return ret;
    }
    that.prevInterestingTime = prevInterestingTime;
    function updateForValue(start, end) {
      if (itemsByStart.length === 0) {
        return;
      }
      var toUpdate = {}, n;
      function mark(item) {
        toUpdate[item.id] = item;
      }
      if (start >= lastStart) {
        n = itemsByStart.length;
        while (ssi + 1 < n && start >= itemsByStart[ssi + 1].start) {
          ssi++;
          if (ssi < n) {
            mark(itemsByStart[ssi]);
          }
        }
        n = itemsByEnd.length;
        while (sei + 1 < n && start >= itemsByEnd[sei + 1].end) {
          sei++;
          if (sei < n) {
            mark(itemsByEnd[sei]);
          }
        }
      } else {
        while (ssi >= 0 && start < itemsByStart[ssi].start) {
          mark(itemsByStart[ssi]);
          ssi--;
        }
        while (sei >= 0 && start < itemsByEnd[sei].end) {
          mark(itemsByEnd[sei]);
          sei--;
        }
      }
      lastStart = opts.copy(start);
      if (end >= lastEnd) {
        n = itemsByStart.length;
        while (esi + 1 < n && end > itemsByStart[esi + 1].start) {
          esi++;
          if (esi < n) {
            mark(itemsByStart[esi]);
          }
        }
        n = itemsByEnd.length;
        while (eei + 1 < n && end >= itemsByEnd[eei + 1].end) {
          eei++;
          if (eei < n) {
            mark(itemsByEnd[eei]);
          }
        }
      } else {
        while (esi >= 0 && end < itemsByStart[esi].start) {
          mark(itemsByStart[esi]);
          esi--;
        }
        while (eei >= 0 && end <= itemsByEnd[eei].end) {
          mark(itemsByEnd[eei]);
          eei--;
        }
      }
      lastEnd = opts.copy(end);
      var item_id;
      for (item_id in toUpdate) {
        if (toUpdate.hasOwnProperty(item_id)) {
          var item = toUpdate[item_id], was_in_range = activeItems[item_id] !== undefined, now_in_range = opts.test(_start, _end, item.start, item.end);
          if (was_in_range != now_in_range) {
            if (now_in_range) {
              activeItems[item_id] = item;
              _enter(item);
            } else {
              delete activeItems[item_id];
              _exit(item);
            }
          }
        }
      }
    }
    function debug() {
      var ret = "";
      ret += "RANGER: " + _start + ", " + _end + "\n";
      ret += "ITEMSBYSTART:\n";
      for (var i = 0; i < itemsByStart.length && i < 10; i++) {
        var item = itemsByStart[i];
        if (ssi == i) {
          ret += "S";
        }
        if (esi == i) {
          ret += "E";
        }
        ret += "[" + item.id + " " + item.start + "-" + item.end + "]\n";
      }
      ret += "ITEMSBYEND:\n";
      for (var i = 0; i < itemsByEnd.length && i < 10; i++) {
        var item = itemsByEnd[i];
        if (sei == i) {
          ret += "S";
        }
        if (eei == i) {
          ret += "E";
        }
        ret += "[" + item.id + " " + item.start + "-" + item.end + "]\n";
      }
      var actives = getActive();
      ret += "ACTIVES: " + actives.length;
      console.log(ret);
    }
    function set(start, end) {
      _start = start;
      _end = end;
      updateForValue(start, end);
      if (opts.debug) {
        debug();
      }
    }
    function getActive() {
      var ret = [];
      for (var item_id in activeItems) {
        if (activeItems.hasOwnProperty(item_id)) {
          ret.push(activeItems[item_id]);
        }
      }
      return ret;
    }
    that.getActive = getActive;
    that.debug = function(val) {
      opts.debug = val;
    };
    that.set = set;
    that.get = function() {
      return currentValue;
    };
    that.min = function() {
      return min;
    };
    that.max = function() {
      return max;
    };
    return that;
  }
  cc.xhr = cc_xhr;
  function cc_xhr(url, responseType, callback) {
    var httpRequest;
    if (window.XMLHttpRequest) {
      httpRequest = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
      try {
        httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
      } catch (e) {
        try {
          httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {}
      }
    }
    httpRequest.onreadystatechange = function() {
      var response;
      if (httpRequest.readyState === 4) {
        if (httpRequest.status === 200) {
          if (responseType == "xml") {
            response = httpRequest.responseXML;
          } else if (responseType == "json") {
            response = JSON.parse(httpRequest.responseText);
          } else {
            response = httpRequest.responseText;
          }
          callback.call(httpRequest, response);
        } else {}
      } else {}
    };
    httpRequest.open("GET", url, true);
    httpRequest.send(null);
    return httpRequest;
  }
  cc.json = cc_json;
  function cc_json(url, callback) {
    return cc_xhr(url, "json", callback);
  }
  cc.$ = cc_$;
  cc.$$ = cc_$$;
  function cc_$(sel, elt) {
    elt = elt == undefined ? document : elt;
    return elt.querySelector(sel);
  }
  function cc_$$(sel, elt) {
    elt = elt == undefined ? document : elt;
    return Array.prototype.slice.call(elt.querySelectorAll(sel));
  }
  cc.timeline = cc_timeline;
  function cc_timeline() {
    var that = {}, curpos = 0;
    that.append = function(x, d) {
      curpos += d;
    };
    that.duration = function() {
      return curpos;
    };
    return that;
  }
  function cc_resource() {}
  function cc_context() {
    var that = {};
    that.glue = function(c1, c2) {};
    that.select;
    return that;
  }
  cc.do_soon = cc_do_soon;
  function cc_do_soon(opts) {
    opts = cc.extend({}, {
      interval: 100,
      delay: 1e3
    }, opts);
    var that = {}, start_time = 0, interval_id = null;
    function cancel() {
      if (interval_id !== null) {
        window.clearInterval(interval_id);
        interval_id = null;
      }
    }
    that.cancel = cancel;
    function do_soon() {
      start_time = new Date().getTime();
      if (interval_id === null) {
        interval_id = window.setInterval(interval, opts.interval);
      }
    }
    that.do_soon = do_soon;
    function do_now() {
      cancel();
      if (opts.action) {
        opts.action.call(that);
      }
    }
    function interval() {
      var elapsed = new Date().getTime() - start_time;
      if (elapsed >= opts.delay) {
        do_now();
      }
    }
    that.pending = function() {
      return interval_id !== null;
    };
    return that;
  }
  cc.zoomable = cc_zoomable;
  function cc_zoomable(elt, opts) {
    var that = {}, dragging = false, last_mouse;
    elt.addEventListener("touchstart", startdrag, false);
    elt.addEventListener("touchcancel", stopdrag, false);
    elt.addEventListener("touchend", stopdrag, false);
    elt.addEventListener("touchmove", move, false);
    elt.addEventListener("mousedown", startdrag, false);
    elt.addEventListener("mouseup", stopdrag, false);
    elt.addEventListener("mouseleave", stopdrag, false);
    elt.addEventListener("mouseout", stopdrag, false);
    elt.addEventListener("mousemove", move, false);
    elt.addEventListener("mousewheel", onwheel, false);
    elt.addEventListener("DOMMouseScroll", onwheel, false);
    that.dragging = function() {
      return dragging;
    };
    function startdrag(e) {
      if (!dragging) {
        dragging = true;
        var me = e.touches ? e.touches[0] : e;
        var mouse = cc.event_mouse_pos(me, elt);
        last_mouse = mouse;
        if (opts.startdrag) {
          opts.startdrag.call(elt, e, mouse);
        }
      }
    }
    function stopdrag(e) {
      if (dragging) {
        dragging = false;
        if (opts.stopdrag) {
          var me = e.touches ? e.touches[0] : e;
          var mouse = cc.event_mouse_pos(me, elt);
          opts.stopdrag.call(elt, e, mouse);
        }
      }
    }
    function move(e) {
      if (dragging) {
        if (opts.drag) {
          var me = e.touches ? e.touches[0] : e;
          var mouse = cc.event_mouse_pos(me, elt);
          mouse.dx = mouse.left - last_mouse.left;
          mouse.dy = mouse.top - last_mouse.top;
          last_mouse = mouse;
          opts.drag.call(elt, e, mouse);
        }
      }
    }
    function onwheel(e) {
      var mouse, ret;
      e = window.event || e;
      var delta = Math.max(-1, Math.min(1, e.wheelDelta || -e.detail));
      if (opts.zoom) {
        mouse = cc.event_mouse_pos(e, elt);
        mouse.zoom = delta < 0 ? -1 : 1;
        ret = opts.zoom.call(elt, e, mouse);
        if (!ret) {
          e.preventDefault();
        }
      }
    }
    return that;
  }
  if (typeof define === "function" && define.amd) define(cc); else if (typeof module === "object" && module.exports) module.exports = cc;
  this.cc = cc;
}();