'use strict';

var fs = require("fs"),
  app = require('http').createServer(handler),
  io = require('socket.io').listen(app),
  nstatic = require('node-static'),
  util = require('util'),
  extend = require("extend"),
  cascadecollage = require("./cascadecollage"),
  spawn = require('child_process').spawn;

var opts = {
  port: 10987
};

/* Extend opts with command line options */
var args = require("minimist")(process.argv.slice(2));
delete(args["_"]);
opts = extend(opts, args);

try {
  extend(opts, JSON.parse(fs.readFileSync("settings.json", "utf8")));
}
catch (e) {
  console.log("warning: unable to read settings.json, using defaults", e)
}

var staticfiles = new(nstatic.Server)(".", { 
  cache: 600, 
  headers: { 'X-Powered-By': 'node-static' } 
});

// console.log("read", opts.;
console.log("Server listening to port " + opts.port);
app.listen(opts.port);

function handler (req, res) {
  // res.writeHead(200);
  // res.end(PAGE); // this doesn't seem to work (original example)
  // console.log("req", req, res);
    staticfiles.serve(req, res, function(err, result) {
      if (err) {
        console.error('Error serving %s - %s', req.url, err.message);
        if (err.status === 404 || err.status === 500) {
          staticfiles.serveFile(util.format('/%d.html', err.status), err.status, {}, req, res);
        } else {
          res.writeHead(err.status, err.headers);
          res.end();
        }
      } else {
        // too much info for the server display
        // console.log('[static] %s', req.url); 
      }
    });
}

var sockets = [];

function new_client_id () {
  var ret = 0,
    ids = {};
  for (var i=0, l=sockets.length; i<l; i++) {
    ids[sockets[i].client_id] = true;
  }
  while(ids[ret] !== undefined) { ret++ };
  return ret;
}

function get_room_list () {
  return sockets.map(function (s) { return s.client_id });  
}

function broadcast (msg, data, exclude) {
  for (var i=0, l=sockets.length; i<l; i++) {
    if (sockets[i] !== exclude) {
      sockets[i].emit(msg, data);
    }
  }  
}

function showimage (path) {
  spawn('scripts/showimage', [path]);
}

function figlet (text) {
  var f = spawn('figlet', [text]);
  f.stdout.setEncoding("utf8")
  f.stdout.on("data", function (data) {
    console.log(data);
  })
}

var cc = cascadecollage(extend(opts, {
  cameraimage: function (path) {
    broadcast("cameraimage", {src: path});
    showimage(path);
  },
  countdown: function (path) {
    broadcast("countdown", {});
  },
  watch: function (path) {
    broadcast("watch", {});
    figlet("watching");
  },
  record: function (path) {
    broadcast("record", {});
    figlet("recording");
  },
  analyze: function (path) {
    broadcast("analyze", {});
    figlet("analyzing");
  },
  match: function (path) {
    broadcast("match", {});
  },
  collage: function (path) {
    broadcast("collage", {});
    figlet("creating collage");
  },  
  cleanup: function (path) {
    broadcast("cleanup", {});
    figlet("cleaning up");
  },  
  imagedata: function (data) {
    broadcast("imagedata", data);
  },
  annotatedcameraframe: function (path) {
    broadcast("annotatedcameraframe", {src: path});
    showimage(path);
  },
  archiveframe: function (img, cf, af) {
    broadcast("archiveframe", {src: img, cf: cf, af:af});
  },
  cameragif: function (img) {
    broadcast("cameragif", {src: img});
  },
  archivegif: function (img) {
    broadcast("archivegif", {src: img});
  },
  collagegif: function (img) {
    broadcast("collagegif", {src: img});
  }
}))

io.sockets.on('connection', function (socket) {
  // client lifetime...
  socket.client_id = new_client_id();
	sockets.push(socket);
	console.log(sockets.length + " active sockets");
	socket.emit("welcome", { client_id: socket.client_id, total_connections: sockets.length, room: get_room_list() })
	broadcast("clientconnect", { client_id: socket.client_id, total_connections: sockets.length, room: get_room_list() }, socket);

	/* Interclient Messaging */
  socket.on("disconnect", function (data) {
    console.log("["+socket.client_id+"] " + "disconnect");
    for (var i=0, l=sockets.length; i<l; i++) {
      if (sockets[i] === socket) {
        sockets.splice(i, 1);
        break;
      }
    }
    socket.emit("goodbye",  { client_id: socket.client_id, total_connections: sockets.length, room: get_room_list() })
    broadcast("clientdisconnect", { client_id: socket.client_id, total_connections: sockets.length, room: get_room_list() }, socket);
    console.log(sockets.length + " active sockets");
  });

  // socket.on('bcast', function (data) {
  //   data.client_id = socket.client_id;
  //   broadcast("bcast", data);
  // });

  // socket.on('privmsg', function (data) {
  //   for (var i=0, l=sockets.length; i<l; i++) {
  //     var s = sockets[i];
  //     if (s.client_id == data.client_id) {
  //       data.client_id = socket.client_id;
  //       s.emit('privmsg', data);
  //       break;
  //     }
  //   }
  // });

})
