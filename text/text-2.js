$( document ).ready(function() {
    var sents=[];
    var curr=0;

    //sents.push("You don’t so much have a face as slide into one.");
    sents.push("On se coule dans un visage plutôt qu'on n'en possède un.");
//    sents.push("A man goes to see his optician and asks for a pair of reading glasses.<br /><br />The optician: I already gave you a new pair last week.<br /><br />The man: I have already read them.");
    sents.push("Un homme va voir son opticien et lui demande des lunettes de lecture.<br /><br/>L'opticien: Je vous en ai déjà donné une nouvelle paire la semaine passée.<br /><br />L'homme: Oui mais je l'ai déjà lue.");
sents.push("<span class='cronenberg'>118<br />00:21:53,880 --> 00:21:58,158<br />Ik voel me naakt.<br />Ik kan mezelf horen.<br /><br />119<br />00:21:58,320 --> 00:22:00,709<br />kan je je stem horen ?<br /><br /></span>");
sents.push("<span class='cronenberg'>120<br />00:22:00,880 --> 00:22:04,839<br />Mooi.<br />- U noemde me een scanner.<br /><br />121<br />00:22:05,000 --> 00:22:07,150<br />Wat is dat ?<br /><br /></span>");
sents.push("<span class='cronenberg'>122<br />00:22:08,600 --> 00:22:14,630<br />Een wangedrocht met<br />een buitenzintuiglijke waarneming.<br /><br />123<br />00:22:14,800 --> 00:22:19,794<br />Een storing van de synaps<br />die we telepathie noemen.<br /><br /></span>");
sents.push("<span class='cronenberg'>124<br />00:22:22,120 --> 00:22:28,639<br />Het kan een ziekte zijn,<br />of een gevolg van straling. Geen idee.<br /><br />125<br />00:22:31,480 --> 00:22:33,391<br />Wie bent u ?<br /><br /></span>");
sents.push("<span class='cronenberg'>126<br />00:22:35,040 --> 00:22:39,158<br />Mijn naam is Dr. Paul Ruth.<br />Ik ben psychofarmacoloog.<br /><br />127<br />00:22:39,320 --> 00:22:43,029<br />Ik ben gespecialiseerd<br />in het fenomeen scanner.<br /><br /></span>");
    sents.push("What kind of madness is it to teach a computer to read not like a human, but like a machine learning to read?"); 
//    sents.push("Did you cut yourself shaving this morning?");
    sents.push("<span class=\"michael\">Visually, the language of the algorithmically annotated image resonates with the vandalized image, the political poster with sprayed-on beard or mustache, the marketing billboard covered with elements crossed out or covered by graffiti tags.</span>");
    sents.push("<span class=\"michael\">There is an essential difference however: where the billboard vandal challenges the authority of the image, the marks of the algorithm carry an authority borne from the often impenetrable layers of technique and software employed. On top of this, such techniques may well be further allied to authority by patents, software licenses, and other aspects of law.</span>");
    sents.push("<span class=\"michael\">Furthermore, the images most frequently subjected to the algorithmic processes of analysis are typically banal ones, themselves collected by agents of authority and typically recorded with their subjects (relatively) unaware of their being taken.</span>");
    sents.push("<span class=\"michael\">For instance, the closed circuit surveillance system in a shopping center or laundromat, or the passport control agent's webcam.</span>");
//    sents.push("I'm a psycho-pharmacist by trade, specializing in the phenomenon of scanners.");
//    sents.push("Je suis un psycho-pharmacien de profession, spécialisé dans le phénomène des scanners.");
    sents.push("Bookworms are of course hermaphrodites, able to turn every punctuation mark into a sex organ.");
//    sents.push("Will she share my love for pornography? Or at least, art? I will let them sniff and sneak through my archives.");
    sents.push("Va-t-elle partager mon amour de la pornographie? Ou au moins de l’art? Je les laisserai fouiner et renifler dans mes archives");
    sents.push("<span class=\"jorn\">The Vendôme column is perhaps one of the most exciting subjects history has to offer comparative vandalism.</span>");
    sents.push("<span class=\"jorn\">Courbet, who was sentenced to pay for the column's reconstruction and declared a vandalist, claimed that the column itself was vandalist in a double sense, as its scale crushed the Place Vendôme and its images glorified Napoleon's belligerent vandalism.</span>");
    sents.push("<span class=\"jorn\">Originally Napoleon himself stood on top as a 3.35 m high Roman Emperor. However, that statue was destroyed as early as 1814 by the Bourbons. In 1831 Louis-Philippe put up a new Napoleon statue, this time naturalist. In 1863 Napoleon III replaced it with a new Caesar, which again has been removed in favor of something else.</span>");
    sents.push("<span class=\"jorn\">As far as vandalism is concerned, there is a difference between the struggle to be on top of everything and dissolving the hierarchy. This last thing was what no one could forgive Courbet, and it remains his great social contribution to art history.</span>");
//    sents.push("You don’t really photograph the present, as the past is woven into it.");
    sents.push("Le lien à la chose en face de l'objectif est toujours là, mais il y a aussi des liens avec des images antérieures qui aident à créer l'image. Vous ne photographiez pas vraiment le présent puisque le passé est tissé en lui.");
    sents.push("A book worm is an avid reader, maybe an amasser of books, a creature with delicate nervous tissue built directly into its eye-mouths, perhaps a voracious chomper shredding pages with its eye-teeth.");
    sents.push("Vous vous êtes coupé en vous rasant ce matin?");
    sents.push("<span class=\"poirot\">A computer vision algorithm is like Hercule Poirot on a crime scene. Where his assistant Captain Hastings will immediately decode the semantic layer, the obvious clues, the motives, Poirot always seems concerned with absurdly pointless details: a slight change of color in a carpet, the position of a finger, etc.</span>");
    sents.push("<span class=\"poirot\">In every Hercule Poirot ending, the tour de force is to make a match between all these apparently absurd little clues and a narrative where the murderer is punished and the social order is confirmed.</span>");
    sents.push("<span class=\"poirot\">In every operation of computer vision, there is this expectation that the gap between human perception and computer perception will be filled. But what if it isn't?</span>");
    sents.push("<span class=\"poirot\">What if it reveals that there is a zone of intersection between human perception and computer vision, and that the contours of this zone are problematic, fluctuating?</span>");
//    sents.push("What have you done to it? What have you done to its eyes?");
    sents.push("Que lui avez-vous fait? Qu'avez-vous fait à ses yeux?");
sents.push("<span class='cronenberg'>128<br />00:22:46,640 --> 00:22:51,191<br />Wat heb je met die vrouw<br />in het winkelcentrum gedaan ?<br /><br />129<br />00:22:51,360 --> 00:22:55,399<br />Ik heb haar niets aangedaan,<br />ze deed het zelf.<br /><br /></span>");
sents.push("<span class='cronenberg'>130<br />00:22:56,360 --> 00:22:58,112<br />Ze...<br /><br />131<br />00:22:59,280 --> 00:23:02,716<br />... dwong me...<br />- Om wat te doen ?<br /><br /></span>");
sents.push("<span class='cronenberg'>132<br />00:23:04,560 --> 00:23:06,755<br />Om aan haar te denken.<br /><br />133<br />00:23:07,760 --> 00:23:12,356<br />Waarom stoorde je je zo aan<br />die vijftig mensen die hier waren ?<br /><br /></span>");
sents.push("<span class='cronenberg'>134<br />00:23:12,520 --> 00:23:15,717<br />Ze praatten te hard.<br />Ze bleven maar praten.<br /><br />135<br />00:23:15,880 --> 00:23:19,395<br />Ik heb hun lippen niet zien bewegen.<br />Jij wel ?<br /><br /></span>");
sents.push("<span class='cronenberg'>136<br />00:23:19,560 --> 00:23:22,950<br />Ze praatten<br />zonder hun lippen te bewegen.<br /><br />137<br />00:23:23,120 --> 00:23:26,749<br />Ik verdronk in de stemmen.<br />Ik kon ze niet stoppen.<br /><br /></span>");
sents.push("<span class='cronenberg'>138<br />00:23:29,040 --> 00:23:34,194<br />Wat gebeurde er met de stemmen<br />toen je die injectie had gekregen ?<br /><br />139<br />00:23:34,360 --> 00:23:39,639<br />Ze hielden op.<br />- Dat medicijn heet ephemerol.<br /><br /></span>");
sents.push("<span class='cronenberg'>140<br />00:23:39,800 --> 00:23:45,079<br />Het onderdrukt de scanactiviteit.<br />Bij normale mensen doet het niets.<br /><br />141<br />00:23:45,240 --> 00:23:49,950<br />Maar bij een scanner<br />houdt het de telepathie tegen.<br /><br /></span>");
sents.push("<span class='cronenberg'>142<br />00:23:50,120 --> 00:23:55,194<br />Je hoort de stemmen dan niet meer.</span>");
    sents.push("<span class=\"bertillon\">Alphonse Bertillon developed not only what have since become the prevailing standards for police portrait photography, he also developed a complex classification system that operated with index cards and which was meant to enable picking a particular individual case out of the enormous number of images contained in the archive.</span>");
    sents.push("<span class=\"bertillon\">In contrast, the anthropologist and eugenicist Francis Galton condensed numerous photographs by superimposing them to create an \"ideal\" composite image, which was meant to cause individual traits to disappear and the characteristics common to the superimposed portraits to manifest</span>.");
    sents.push("<span class=\"bertillon\">While Bertillon was concerned with the unambiguous identification of a person, Galton was occupied with typification intended to help recognize criminals.</span>");
    sents.push("<span class=\"bertillon\">Bertillon sought to embed the photograph in the archive. Galton sought to embed the archive in the photograph.</span>");
//   sents.push("... like four-eye machines made of elementary faces linked together two by two.");
    sents.push("... comme des machines à quatre yeux qui sont des visages élémentaires liés deux par deux.");
    sents.push("Everyday usage and parlance arrests memory and its degenerative possibilities in order to support dreams of superhuman digital programmability. These dreams create, rather than solve, archival nightmares. They proliferate nonsimultaneous enduring ephemerals.");


    function startReading() {
        reading();
	lpp = setInterval(reading, 15000);
    }

    function reading(){
	console.log("entering read");
	$('#screen').html(sents[curr]);
	if(curr<(sents.length-1)){
	    curr++;
	}else{
	    curr=0;
	}
    }
    startReading();
});

