document.addEventListener("DOMContentLoaded", function () {
	var sockaddr = window.location.protocol+'//'+window.location.host,
		socket,
		img = document.querySelector("img"),
		video = document.querySelector("video"),
		msg = document.getElementById("message"),
		archivegif;

	img.style.display = "none";		
	video.style.display = "none";		
	console.log("opening connection to", sockaddr);
	socket = io.connect();
	socket.on("connect", function (data) {
		console.log("connected to socket server");
	});
	socket.on("watch", function (data) {
		msg.innerHTML = "";
	});
	socket.on("record", function (d) {
		img.style.display = "none";
		msg.innerHTML = "";
	})
	socket.on("collage", function (d) {
		msg.innerHTML = "Creating collage...";
		img.style.display = "none";
		video.style.display = "none";
	})
	socket.on("archiveframe", function (d) {
		var now = new Date(),
			tstr = ""+now.getFullYear()+now.getMonth()+now.getDay()+now.getHours()+now.getMinutes()+now.getSeconds(),
			src = d.src + "?t=" + tstr;
		img.src = src;
		// console.log("archiveframe", src, d);
		var feature_name = (d.af.feature == "face") ? "Face" : "Eye";
		// msg.innerHTML = feature_name + " detected";
		msg.innerHTML = "Matching with faces & eyes <br>from the archive";
		img.style.display = "block";
		video.pause();
		video.style.display = "none";
	});
	// commenting out -- allow loop to remain until actual matching occurs (via archiveframe)
	// socket.on("analyze", function () {
	// 	// start a countdown...
	// 	msg.innerHTML = "Searching for faces <br>from the archive...";
	// 	img.style.display = "none";
	// 	video.pause();
	// 	video.style.display = "none";
	// })
	socket.on("archivegif", function (d) {
		console.log("archivegif", d.src);
		archivegif = d.src;
		// msg.innerHTML = "";
		// 
		// img.style.display = "block";
		// img.src = d.src;
	});
	socket.on("collagegif", function (d) {
		// console.log("collagegif", d.src);
		msg.innerHTML = "";
		// img.src = archivegif;
		img.style.display = "none";
		video.src = archivegif;
		video.style.display = "block";
	});
})
