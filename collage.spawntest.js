var spawn = require('child_process').spawn,
	path = "collage/creatingcollage.mp4",
	// params = ['-loop', '0', path],
	params = ['--loop', path],
	// params = ['-fs', '-loop', '0', path],	
	// mplayer = spawn('mplayer', params);
	player = spawn('cvlc', params);

player.on("close", function (code) {
	console.log("player closed with code", code);
});
// ok killing a spawned process doesn't seem to be a problem... moving on...

setTimeout(function () {
	player.kill();
	setTimeout(function () {
		player.kill();
		console.log("second kill");
		setTimeout(function () {
			console.log("that's all folks");
		}, 5000);
	}, 5000);
}, 10000);

